package fr.upem.projet.algo.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import fr.upem.projet.algo.obstacles.graph.Graph;
import fr.upem.projet.algo.obstacles.graph.Vertex;
import fr.upem.projet.algo.obstacles.map.Map;

public class OutputImage {

	/**
	 * Generate the PNG file with using the graph.
	 * 
	 * @param file
	 *            The file.
	 * @param map
	 *            The map.
	 * @param graph
	 *            The graph.
	 */
	public static void savePNG(File file, Map map, Graph graph) {
		map.drawGraph(graph);
		map.drawObstacles();
		try {
			ImageIO.write(map.getBi(), "png", file);
		} catch (IOException e) {
			System.err
					.println("IOException : Error during the generation of the output image.");
		}
		System.out.println("The output image " + file.getName()
				+ " was generated in the folder output.");
	}

	/**
	 * Generate the PNG file with using the graph and the draw the shortest
	 * path.
	 * 
	 * @param file
	 *            The file.
	 * @param map
	 *            The map.
	 * @param graph
	 *            The graph.
	 * @param shortestPath
	 * 				The shortestPath
	 */
	public static void savePNGWithShortestPath(File file, Map map, Graph graph,
			List<Vertex> shortestPath) {
		map.drawGraphShortestPath(graph, shortestPath);
		map.drawObstacles();
		try {
			ImageIO.write(map.getBi(), "png", file);
		} catch (IOException e) {
			System.err
					.println("IOException : Error during the generation of the output image.");
		}
		System.out.println("The output image " + file.getName()
				+ " was generated in the folder output.");
	}

}
