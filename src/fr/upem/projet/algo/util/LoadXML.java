package fr.upem.projet.algo.util;

import java.io.File;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import fr.upem.projet.algo.obstacles.map.Map;
import fr.upem.projet.algo.obstacles.map.Obstacle;
import fr.upem.projet.algo.obstacles.map.Point;

/**
 * Manage the Loading XML.
 * 
 * @author Jérémy LOR (jlor@etudiant.univ-mlv.fr)
 * @author Maxime DESHAGETTE (mdeshage@etudiant.univ-mlv.fr)
 */
public class LoadXML {

	/**
	 * The XML document.
	 */
	private static Document document;

	/**
	 * The racine element in the XML file.
	 */
	private static Element racine;

	/**
	 * Load an XML with a given filename.
	 * 
	 * @param filename
	 *            The name of the file.
	 * @return the Graph.
	 */
	public static Map load(String filename) {

		// Create an instance of SaxBuilder.
		SAXBuilder sxb = new SAXBuilder();
		try {

			// We create a new JDOM document with the given xml file (in the
			// folder data).
			document = sxb.build(new File("xml/" + filename));

		} catch (Exception e) {
			System.out.println("File not found : " + e.getLocalizedMessage());
		}

		// Now we initialize a new root element with the given root element of
		// the document.
		racine = document.getRootElement();

		// We get the children of the root.
		Map map = null;
		List<Element> ListRectangle = racine.getChildren("rectangle");
		List<Element> ListPoints = racine.getChildren("points");

		for (Element r : ListRectangle) {
			List<Element> dimension = r.getChildren("dimension");
			for (Element d : dimension) {
				int height = Integer.valueOf(d.getAttributeValue("height"));
				int width = Integer.valueOf(d.getAttributeValue("width"));
				map = new Map(width, height);
			}
			List<Element> obstacles = r.getChildren("obstacles");
			for (Element obs : obstacles) {
				List<Element> ListObstacles = obs.getChildren("obstacle");
				for (Element o : ListObstacles) {
					int topLeftX = Integer.valueOf(o
							.getAttributeValue("topleftx"));
					int topLeftY = Integer.valueOf(o
							.getAttributeValue("toplefty"));
					int bottomRightX = Integer.valueOf(o
							.getAttributeValue("bottomrightx"));
					int bottomRightY = Integer.valueOf(o
							.getAttributeValue("bottomrighty"));
					Obstacle obstacle = new Obstacle(topLeftX, topLeftY,
							bottomRightX, bottomRightY);
					map.addObstacle(obstacle);
				}
			}
		}
		for (Element p : ListPoints) {
			int startX = 0;
			int startY = 0;
			int endX = 0;
			int endY = 0;
			List<Element> start = p.getChildren("start");
			for (Element s : start) {
				startX = Integer.valueOf(s.getAttributeValue("x"));
				startY = Integer.valueOf(s.getAttributeValue("y"));
				Point pStart = new Point(startX, startY);
				map.addPoints(pStart);
			}
			List<Element> end = p.getChildren("end");
			for (Element e : end) {
				endX = Integer.valueOf(e.getAttributeValue("x"));
				endY = Integer.valueOf(e.getAttributeValue("y"));
				Point pEnd = new Point(endX, endY);
				map.addPoints(pEnd);
			}
		}

		return map;

	}
}
