package fr.upem.projet.algo.obstacles.dijkstra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

import fr.upem.projet.algo.obstacles.graph.Edge;
import fr.upem.projet.algo.obstacles.graph.Vertex;

/**
 * Djikstra Algorithm - Get the shortest path from a Vertex start to a Vertex
 * end.
 * 
 * @author Jérémy LOR (jlor@etudiant.univ-mlv.fr)
 * @author Maxime DESHAGETTE (mdeshage@etudiant.univ-mlv.fr)
 * 
 */
public class Dijkstra {

	/**
	 * Use the Dijkstra Algorithm with a PriorityQueue<Vertex>
	 * 
	 * @param start
	 *            The Vertex start.
	 */
	public static void applyDijkstra(Vertex start) {
		start.setMinDistance(0.);
		PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
		vertexQueue.add(start);

		while (!vertexQueue.isEmpty()) {
			Vertex u = vertexQueue.poll();

			// We visited neighbours of the vertex u.
			for (Edge e : u.getNeighbours()) {
				Vertex v = e.getEnd();
				double weight = e.getWeight();
				double distanceThroughU = u.getMinDistance() + weight;
				if (distanceThroughU < v.getMinDistance()) {
					vertexQueue.remove(v);
					v.setMinDistance(distanceThroughU);
					v.setPrevious(u);
					vertexQueue.add(v);
				}
			}
		}
	}

	/**
	 * Return a list of the shortest path to the Vertex end.
	 * 
	 * @param end
	 *            The Vertex end.
	 * @return The List of the shortest path.
	 */
	public static List<Vertex> getShortestPathTo(Vertex end) {
		List<Vertex> path = new ArrayList<Vertex>();
		for (Vertex vertex = end; vertex != null; vertex = vertex.getPrevious()) {
			path.add(vertex);
		}
		Collections.reverse(path);
		return path;
	}
}
