package fr.upem.projet.algo.obstacles.map;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import fr.upem.projet.algo.obstacles.graph.Graph;
import fr.upem.projet.algo.obstacles.graph.Vertex;

/**
 * Represent the map (The entire graph).
 * 
 * @author Jérémy LOR (jlor@etudiant.univ-mlv.fr)
 * @author Maxime DESHAGETTE (mdeshage@etudiant.univ-mlv.fr)
 */
public class Map {
	private final int dimensionX;
	private final int dimensionY;
	private BufferedImage bi;
	private Set<Vertex> obstacles;
	private List<Point> points;

	/**
	 * Constructor
	 * 
	 * @param dimensionX
	 * 		width of the map
	 * @param dimensionY
	 * 		height of the map
	 */
	public Map(int dimensionX, int dimensionY) {
		this.points = new ArrayList<Point>();
		this.obstacles = new HashSet<>();
		this.dimensionX = dimensionX;
		this.dimensionY = dimensionY;
		this.bi = new BufferedImage(dimensionX, dimensionY,
				BufferedImage.TYPE_INT_ARGB);
	}

	/**
	 * Add a point at the list "point"
	 * 
	 * @param p
	 * 		point to add
	 * @return
	 * 		true if the add is correct, else false
	 */
	public boolean addPoints(Point p) {
		Objects.requireNonNull(p);
		if (points.add(p)) {
			return true;
		}
		return false;
	}

	/**
	 * Add a obstacle at the list "obstacle"
	 * 
	 * @param obstacle
	 * 		obstacle to add
	 * 
	 * @return
	 */
	public boolean addObstacle(Obstacle obstacle) {
		Objects.requireNonNull(obstacle);
		int topLeftX = obstacle.getTopLeftX();
		int topLeftY = obstacle.getTopLeftY();
		int bottomRightX = obstacle.getBottomRightX();
		int bottomRightY = obstacle.getBottomRightY();

		for (int j = topLeftY; j <= bottomRightY; j++) {
			for (int i = topLeftX; i <= bottomRightX; i++) {
				Vertex vertex = new Vertex(i, j);
				vertex.setColor(Color.black);
				this.obstacles.add(vertex);
			}
		}
		return true;
	}

	/**
	 * Draw a graph
	 * 
	 * @param graph
	 * 		graph to draw
	 */
	public void drawGraph(Graph graph) {
		Vertex[] arrayV = graph.getArrayVertex();
		for (int i = 0; i < arrayV.length; i++) {
			bi.setRGB(arrayV[i].getX(), arrayV[i].getY(), arrayV[i].getColor()
					.getRGB());
		}
	}

	/**
	 * Draw a graph with the shortest path
	 * 
	 * @param graph
	 * 		graph to draw
	 * @param shortestPath
	 * 		shortest path to draw
	 */
	public void drawGraphShortestPath(Graph graph, List<Vertex> shortestPath) {
		Vertex[] arrayV = graph.getArrayVertex();
		for (int i = 0; i < arrayV.length; i++) {
			bi.setRGB(arrayV[i].getX(), arrayV[i].getY(), arrayV[i].getColor()
					.getRGB());
		}
		for (Vertex v : shortestPath) {
			if (v.getColor() != Color.RED) {
				bi.setRGB(v.getX(), v.getY(), Color.BLUE.getRGB());
			}
		}
	}

	/**
	 * Draw obstacles
	 */
	public void drawObstacles() {
		for (Vertex v : obstacles) {
			bi.setRGB(v.getX(), v.getY(), Color.BLACK.getRGB());
		}
	}

	/**
	 * Get the buffered image
	 * @return
	 * 		buffered image
	 */
	public BufferedImage getBi() {
		return bi;
	}

	/**
	 * Get the list of the points
	 * @return
	 * 		list of points
	 */
	public List<Point> getPoints() {
		return points;
	}

	/**
	 * Get the obstacles
	 * @return
	 * 		obstacles
	 */
	public Set<Vertex> getObstacles() {
		return obstacles;
	}

	/**
	 * Get the width of the map
	 * @return
	 * 		width of the map
	 */
	public int getDimensionX() {
		return dimensionX;
	}

	/**
	 * Get the height of the map
	 * @return
	 * 		height of the map
	 */
	public int getDimensionY() {
		return dimensionY;
	}
}
