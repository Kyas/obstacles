package fr.upem.projet.algo.obstacles.map;

/**
 * 
 * @author Jérémy LOR (jlor@etudiant.univ-mlv.fr)
 * @author Maxime DESHAGETTE (mdeshage@etudiant.univ-mlv.fr)
 */
public class Obstacle {
	private final int topLeftX;
	private final int topLeftY;
	private final int bottomRightX;
	private final int bottomRightY;

	/**
	 * Constructor
	 * 
	 * @param topLeftX
	 * 		topleft x of the obstacles
	 * @param topLeftY
	 * 		topleft  y of the obstacles
	 * @param bottomRightX
	 * 		bottomRight x of the obstacles
	 * @param bottomRightY
	 * 		bottomRight y of the obstacles
	 */
	public Obstacle(int topLeftX, int topLeftY, int bottomRightX,
			int bottomRightY) {
		this.topLeftX = topLeftX;
		this.topLeftY = topLeftY;
		this.bottomRightX = bottomRightX;
		this.bottomRightY = bottomRightY;
	}

	/**
	 * Display the obstacle into a string
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Obstacle : [").append("(").append(topLeftX).append(",")
				.append(topLeftY).append("),(").append(bottomRightX)
				.append(",").append(bottomRightY).append(")]");
		return sb.toString();
	}
	
	/**
	 * Get the topleft x of the obstacles
	 * 
	 *  @return
	 *  	topleftX
	 */
	public int getTopLeftX() {
		return topLeftX;
	}
	
	/**
	 * Get the topleft y of the obstacles
	 * 
	 *  @return
	 *  	topleftY
	 */
	public int getTopLeftY() {
		return topLeftY;
	}
	
	/**
	 * Get the bottomRight x of the obstacles
	 * 
	 *  @return
	 *  	bottoRightX
	 */
	public int getBottomRightX() {
		return bottomRightX;
	}
	
	/**
	 * Get the bottomRight Y of the obstacles
	 * 
	 *  @return
	 *  	bottoRightY
	 */
	public int getBottomRightY() {
		return bottomRightY;
	}
}
