package fr.upem.projet.algo.obstacles.map;

/**
 * 
 * @author Jérémy LOR (jlor@etudiant.univ-mlv.fr)
 * @author Maxime DESHAGETTE (mdeshage@etudiant.univ-mlv.fr)
 */
public class Point {
	private final int x;
	private final int y;

	/**
	 * Constructor
	 * 
	 * @param x
	 * 		x position of the point
	 * @param y
	 * 		y position of the point
	 */
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Display a point into a string
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Point : ").append("(").append(x).append(",").append(y)
				.append(")");
		return sb.toString();
	}

	/**
	 * Get the x position of the point
	 * @return
	 * 		x position of the point
	 */
	public int getX() {
		return x;
	}

	/**
	 * Get the y position of the point
	 * @return
	 * 		y position of the point
	 */
	public int getY() {
		return y;
	}

}
