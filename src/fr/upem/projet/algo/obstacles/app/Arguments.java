package fr.upem.projet.algo.obstacles.app;

import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.BooleanOptionHandler;

/**
 * Manage Options and Arguments. (Use the Args4j library).
 * 
 * @author Jérémy LOR (jlor@etudiant.univ-mlv.fr)
 * @author Maxime DESHAGETTE (mdeshage@etudiant.univ-mlv.fr)
 */

public class Arguments {

	/**
	 * Option help.
	 */
	@Option(name = "-h", aliases = "--help", handler = BooleanOptionHandler.class, usage = "display the help (how this program is used).")
	private boolean help = false;

	/**
	 * Option input.
	 */
	@Option(name = "-i", aliases = "--input", required = true, usage = "Input the XML file.")
	private String input;

	/**
	 * Option output.
	 */
	@Option(name = "-o", aliases = "--output", required = true, usage = "Output the PNG image.")
	private String output;

	/**
	 * Option With-Shortest-Path.
	 */
	@Option(name = "-w", aliases = "--with-shortest-path", handler = BooleanOptionHandler.class, usage = "Use the Dijkstra algorithm.")
	private boolean withShortestPath = false;

	/**
	 * Option output.
	 */
	@Option(name = "-r", aliases = "--reduction", handler = BooleanOptionHandler.class, usage = "Reduce the graph before using Dijkstra algorithm.")
	private boolean reduction = false;

	/**
	 * Check if the option help is here.
	 * 
	 * @return <code>true</code> if the option is here, <code>false</code>
	 *         otherwise.
	 */
	public boolean isHelp() {
		return help;
	}

	/**
	 * Check if the option input is here.
	 * 
	 * @return the input file..
	 */
	public String getInput() {
		return input;
	}

	/**
	 * Check if the option output is here.
	 * 
	 * @return <code>true</code> if the option is here, <code>false</code>
	 *         otherwise.
	 */
	public String getOutput() {
		return output;
	}

	/**
	 * Check if the option withShortestPath is here.
	 * 
	 * @return <code>true</code> if the option is here, <code>false</code>
	 *         otherwise.
	 */
	public boolean isWithShortestPath() {
		return withShortestPath;
	}

	/**
	 * Check if the option reduction is here.
	 * 
	 * @return <code>true</code> if the option is here, <code>false</code>
	 *         otherwise.
	 */
	public boolean isReduction() {
		return reduction;
	}

}
