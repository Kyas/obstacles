package fr.upem.projet.algo.obstacles.app;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import fr.upem.projet.algo.obstacles.dijkstra.Dijkstra;
import fr.upem.projet.algo.obstacles.graph.Graph;
import fr.upem.projet.algo.obstacles.graph.Vertex;
import fr.upem.projet.algo.obstacles.map.Map;
import fr.upem.projet.algo.util.LoadXML;
import fr.upem.projet.algo.util.OutputImage;

/**
 * Main Application of the Project.
 * 
 * @author Jérémy LOR (jlor@etudiant.univ-mlv.fr)
 * @author Maxime DESHAGETTE (mdeshage@etudiant.univ-mlv.fr)
 */
public class Main {

	public static String FOLDER_OUTPUT = "output/";

	/**
	 * Display the Help Notice.
	 */
	public static void displayHelp() {
		StringBuilder sb = new StringBuilder();
		System.out
				.println(sb
						.append("\n== Obstacles - Project  ==")
						.append("\n== HELP NOTICE ==\n")
						.append("How to use this program :\n")
						.append("\t-- OPTIONS --\n")
						.append("\t-h --help : Display the help notice.\n")
						.append("\t-i --input : Input the XML file.\n")
						.append("\t-o --output : Output the PNG image.\n")
						.append("\t-w --with-shortest-path : Use the Dijkstra Algorithm.\n")
						.append("\t-r --reduction : Reduce the graph before using the Dijkstra Algorithm.\n")
						.append("\t-- ARGUMENT --\n")
						.append("\t<file> Name of the file with .xml format (mandatory)\n\n")
						.append("\t-- EXAMPLE --\n")
						.append("\tjava Obstacles --with-shortest-path -r -i input.xml -o output.png\n")
						.toString());
	}

	/**
	 * Main method.
	 * 
	 * @param args
	 * 	
	 * @throws IOException
	 * 
	 */
	public static void main(String[] args) throws IOException {

		// With the command line.
		final Arguments options = new Arguments();
		CmdLineParser parser = new CmdLineParser(options);

		parser.setUsageWidth(80);

		try {
			// Parsing arguments.
			parser.parseArgument(args);

			if (args == null || args.length < 1) {
				System.out
						.println("Not enough arguments ! Please use the correct format");
				displayHelp();
				System.exit(1);
			} else {
				try {

					if (options.isHelp()) {
						displayHelp();
					} else {

						// TIMER FOR THE PROCESSING
						long startTime = System.currentTimeMillis();

						if (options.getInput().isEmpty()
								|| options.getOutput().isEmpty()) {
							System.out.println("Options -i or -o isn't here !");
							displayHelp();
							System.exit(1);
						} else {
							if (!options.getInput().endsWith(".xml")) {
								System.err
										.println("The input file is not a XML file !");
								System.exit(1);
							}
							if (!options.getOutput().endsWith(".png")) {
								System.err
										.println("The output file is not a PNG file !");
								System.exit(1);
							}
							System.out.println("Input : " + options.getInput()
									+ ", ShortestPath : "
									+ options.isWithShortestPath()
									+ ", Output : " + options.getOutput()
									+ ", Reduction : " + options.isReduction());
							System.out.println("== START PROCESSING==\n");

							/**
							 * Parsing the .XML and generating the graph.
							 */
							Map map = LoadXML.load(options.getInput());
							Graph graph = new Graph(map.getDimensionX()
									* map.getDimensionY());
							/**
							 * Reduction graph
							 */
							if (options.isReduction()) {
								System.out
										.println("\n== REDUCTION Graph is not implemented here\n"
												+ "(The actual implementation is better)... ==\n");
							} else {
								graph.addMap(map);
							}

							// To get the output file after.
							File file = new File(FOLDER_OUTPUT
									+ options.getOutput());

							/**
							 * Use Dijkstra Algorithm.
							 */
							if (options.isWithShortestPath()) {
								System.out
										.println("\n== DIJKSTRA ALGORITHM in progress... ==\n");
								Dijkstra.applyDijkstra(graph.getStart());
								List<Vertex> path = new ArrayList<>();
								System.out.println("Graph start : "
										+ graph.getStart());
								System.out.println("Graph end : "
										+ graph.getEnd());
								path = Dijkstra.getShortestPathTo(graph
										.getEnd());
								System.out.println("Path: " + path);

								// Generate the output image with the shortest
								// path.
								OutputImage.savePNGWithShortestPath(file, map,
										graph, path);
							} else {
								/**
								 * Without Dijkstra Algorithm.
								 */
								System.out
										.println("\n== GENERATING IMAGE WITHOUT DIJKSTRA... ==\n");
								// Generate the output image without the
								// shortest path.
								System.out.println("Graph start : "
										+ graph.getStart());
								System.out.println("Graph end : "
										+ graph.getEnd());
								OutputImage.savePNG(file, map, graph);
							}
						}

						long finishTime = System.currentTimeMillis();
						long elapsedTime = (finishTime - startTime) / 1000;
						System.out.println("Time estimated : " + elapsedTime
								+ " seconds");
						System.out.println("\n== END PROCESSING ==");
					}
				} catch (NullPointerException e) {

					// We do nothing here, the method load from LoadXML does the
					// job.
					return;
				}

			}
		} catch (CmdLineException e) {
			// If there is an error during the using command line.
			System.err.println("Error in the command line !");
			displayHelp();
			System.exit(1);
		}
	}
}
