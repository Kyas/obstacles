package fr.upem.projet.algo.obstacles.graph;

public class Reduction {
	static int setWeight(int x1, int y1, int x2, int y2) {
		// h(s1,s2) = max(abs(x1 - x2),abs(y1-y2))
		return Math.max(Math.abs(x1 - x2), Math.abs(y1 - y2));
	}

	static boolean visible(int ux, int uy, int vx, int vy, int ax, int ay,
			int bx, int by) {
		// ux, uy, vx, vy sont les coordonnees des sommets u et v
		// ax, ay, bx, by sont des coordonnees d'un obstacle
		// resultat : vrai si v est visible par u par rapport a l'obstacle
		boolean test;
		test = intersection(2 * ux + 1, 2 * uy + 1, 2 * vx + 1, 2 * vy + 1,
				2 * ax, 2 * ay, 2 * bx + 2, 2 * by + 2)
				|| intersection(2 * ux + 1, 2 * uy + 1, 2 * vx + 1, 2 * vy + 1,
						2 * ax, 2 * by + 2, 2 * bx + 2, 2 * ay);
		return !test;
	}

	static boolean intersection(int ax, int ay, int bx, int by, int cx, int cy,
			int dx, int dy) {
		int t1 = Integer.signum((cx - ax) * (by - ay) - (bx - ax) * (cy - ay));
		int t2 = Integer.signum((dx - ax) * (by - ay) - (bx - ax) * (dy - ay));

		if (t1 * t2 >= 0) {
			return false;
		}

		int t3 = Integer.signum((ax - cx) * (dy - cy) - (dx - cx) * (ay - cy));
		int t4 = Integer.signum((bx - cx) * (dy - cy) - (dx - cx) * (by - cy));

		return (t3 * t4 < 0);
	}
}
