package fr.upem.projet.algo.obstacles.graph;

/**
 * Manage Edges (Couple of Vertices).
 * 
 * @author Jérémy LOR (jlor@etudiant.univ-mlv.fr)
 * @author Maxime DESHAGETTE (mdeshage@etudiant.univ-mlv.fr)
 */
public class Edge {

	/**
	 * The vertex's start.
	 */
	private Vertex start;
	/**
	 * The vertex's end.
	 */
	private Vertex end;

	/**
	 * The vertex's weight.
	 */
	private final double weight;

	/**
	 * The constructor.
	 * 
	 * @param start
	 *            The vertex's start.
	 * @param end
	 *            The vertex's end.
	 * @param weight
	 * 			The vertex's weight
	 */
	public Edge(Vertex start, Vertex end, double weight) {
		this.start = start;
		this.end = end;
		this.weight = weight;
	}

	/**
	 * The constructor.
	 * 
	 * @param start
	 *            The vertex's start.
	 * @param end
	 *            The vertex's end.
	 */
	public Edge(Vertex start, Vertex end) {
		this(start, end, 1);
	}

	/**
	 * Display the edge into a String.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\t" + start + " -- " + end);
		return sb.toString();
	}

	/**
	 * Get the vertex's start.
	 * 
	 * @return The start.
	 */
	public Vertex getStart() {
		return start;
	}

	/**
	 * Get the vertex's end.
	 * 
	 * @return The end.
	 */
	public Vertex getEnd() {
		return end;
	}

	/**
	 * Get the vertex's weight.
	 * 
	 * @return The weight.
	 */
	public double getWeight() {
		return weight;
	}
}
