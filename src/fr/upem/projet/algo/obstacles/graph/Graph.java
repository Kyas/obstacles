package fr.upem.projet.algo.obstacles.graph;

import java.awt.Color;
import java.util.Iterator;
import java.util.List;

import fr.upem.projet.algo.obstacles.map.Map;
import fr.upem.projet.algo.obstacles.map.Point;

/**
 * Creation of a Graph with ponderation and non-orientation.
 * 
 * @author Jérémy LOR (jlor@etudiant.univ-mlv.fr)
 * @author Maxime DESHAGETTE (mdeshage@etudiant.univ-mlv.fr)
 */
public class Graph {

	/**
	 * Number of Vertices.
	 */
	private final int nbVertices;

	/**
	 * The Vertex's Array.
	 */
	private Vertex[] arrayVertex;

	/**
	 * The Vertex start.
	 */
	private Vertex start;

	/**
	 * The Vertex end.
	 */
	private Vertex end;

	/**
	 * The constructor.
	 * 
	 * @param nbVertices
	 *            The number of Vertices.
	 */
	public Graph(int nbVertices) {
		this.nbVertices = nbVertices;
		this.arrayVertex = new Vertex[nbVertices];
	}

	/**
	 * Calcul the edges's number 
	 * 
	 * @return  nbEdges
	 * 			The number of edges
	 */
	public int nbEdges() {
		int nbEdges = 0;
		for (int i = 0; i < arrayVertex.length; i++) {
			if (!arrayVertex[i].getNeighbours().isEmpty()) {
				nbEdges += arrayVertex[i].getNeighbours().size();
			}
		}
		return nbEdges;
	}

	/**
	 * Create the table containing the different Vertex
	 * 
	 * @param map (The entire graph)
	 */
	public void addMap(Map map) {
		int xLength = map.getDimensionX();
		int yLength = map.getDimensionY();
		int index;

		List<Point> points = map.getPoints();
		for (int i = 0; i < xLength; i++) {
			for (int j = 0; j < yLength; j++) {
				index = (j * xLength) + i;
				Vertex v = new Vertex(i, j);

				for (Point p : points) {
					if (i == p.getX() && j == p.getY()) {
						v.setColor(Color.RED);
						if (start == null) {
							start = v;
						} else if (end == null) {
							end = v;
						}
					}
				}
				arrayVertex[index] = v;
			}
		}

		//long time1 = System.currentTimeMillis();
		//int nb = 0;
		for (int k = 0; k < arrayVertex.length; k++) {
			arrayVertex[k].addNeighbours(this, map, k, arrayVertex);
			//nb = k;
		}
		//long time2 = System.currentTimeMillis();
		//System.out.println("K : " + nb + " Time : " + (time2 - time1)
		//		+ " ms Moyenne : " + (float) (time2 - time1) / nb
		//		+ " ms de traitement de addNeighbour");

	}

	/**
	 * Display the graph into a String
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder().append("graph G {\n");
		for (int i = 0; i < nbVertices; i++) {
			if (!arrayVertex[i].getNeighbours().isEmpty()) {
				sb.append(i + " : " + arrayVertex[i] + "\n");

				for (int j = 0; j < arrayVertex[i].getNeighbours().size(); j++) {
					sb.append(" \t -> "
							+ arrayVertex[i].getNeighbours().get(j).getEnd());
					sb.append("\n");
				}
			}
		}
		return sb.append("}").toString();
	}
	
	/**
	 * An Iterator for the neighbours
	 * 
	 * @param source
	 * 
	 * @return neighbours
	 */
	public Iterator<Edge> neighbours(final Vertex source) {
		return new Iterator<Edge>() {
			Iterator<Edge> it = source.getNeighbours().listIterator();

			@Override
			public boolean hasNext() {
				return it.hasNext();
			}

			@Override
			public Edge next() {
				return it.next();
			}
		};
	}

	/**
	 * Get the vertex's table containing the full vertex
	 * 
	 * @return
	 * 		arrayVertex
	 */
	public Vertex[] getArrayVertex() {
		return arrayVertex;
	}

	/**
	 * Get the number of vertices
	 * 
	 * @return
	 * 		nbVertices
	 */
	public int getNbVertices() {
		return nbVertices;
	}

	/**
	 * Get the start vertex
	 * 
	 * @return 
	 * 		start
	 */
	public Vertex getStart() {
		return start;
	}

	/**
	 * Get the end vertex
	 * 
	 * @return
	 * 		end
	 */
	public Vertex getEnd() {
		return end;
	}
}
