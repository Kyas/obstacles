package fr.upem.projet.algo.obstacles.graph;

import java.awt.Color;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import fr.upem.projet.algo.obstacles.map.Map;

/**
 * Manage Vertex with its id, color and its neighbours (list of Edges).
 * 
 * @author Jérémy LOR (jlor@etudiant.univ-mlv.fr)
 * @author Maxime DESHAGETTE (mdeshage@etudiant.univ-mlv.fr)
 */
public class Vertex implements Comparable<Vertex> {

	/**
	 * The coordinate x.
	 */
	private final int x;

	/**
	 * The coordinate y.
	 */
	private final int y;

	/**
	 * The color of the Vertex.
	 */
	private Color color;

	/**
	 * The minimum distance.
	 */
	private double minDistance = Double.POSITIVE_INFINITY;

	/**
	 * The previous Vertex.
	 */
	private Vertex previous;

	/**
	 * List of edges which contains neighbours of the Vertex Id.
	 */
	private final List<Edge> neighbours;

	/**
	 * Define a constant to represent an empty color, which is
	 * Integer.MAX_VALUE.
	 */
	private static final Color EMPTYCOLOR = Color.WHITE;

	/**
	 * Constructor depending of the vertex id.
	 * 
	 * @param id
	 *            The vertex id.
	 * @param x
	 * 			The x position of the vertex
	 * @param y
	 * 			The y position of the vertex
	 */
	public Vertex(int x, int y) {
		this.x = x;
		this.y = y;
		this.color = EMPTYCOLOR;
		this.neighbours = new LinkedList<Edge>();
	}

	/**
	 * Add the neighbours at each vertex
	 * 
	 * @param graph
	 * 			graph
	 * @param map
	 * 			map
	 * @param indice
	 * 			indice of this vertex
	 * @param arrayVertex
	 * 			array contains the full vertex
	 */
	public void addNeighbours(Graph graph, Map map, int indice,
			Vertex[] arrayVertex) {

		// To get the reference in the Neighbours
		// (retrieve the right Vertex in edges).
		Set<Vertex> obstacle = map.getObstacles();
		int maxX = map.getDimensionX();
		int maxY = map.getDimensionY();
		for (int i : this.getIndiceEightNeighbour(maxX)) {
			if (i < 0 || i >= maxX * maxY) {
				continue;
			}
			if (obstacle.contains(arrayVertex[i])) {
				continue;
			}
			neighbours.add(new Edge(this, arrayVertex[i]));
		}
	}

	/**
	 * Get vertex's x position
	 * 
	 * @return
	 * 		x
	 */
	public int getX() {
		return x;
	}

	/**
	 * Get vertex's y position
	 * 
	 * @return
	 * 		y
	 */
	public int getY() {
		return y;
	}

	/**
	 * Get the color of the Vertex.
	 * 
	 * @return The color of the Vertex.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Get the minimum distance of the Vertex.
	 * 
	 * @return The distance of the Vertex.
	 */
	public double getMinDistance() {
		return minDistance;
	}

	/**
	 * Set the minimum distance of the Vertex.
	 * 
	 * @param minDistance
	 *            The minimum distance.
	 */
	public void setMinDistance(double minDistance) {
		this.minDistance = minDistance;
	}

	/**
	 * Set a color in the Vertex with a given color.
	 * 
	 * @param color
	 *            The color of the Vertex.
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Get the color into the string
	 * @return
	 * 		string color
	 */
	public String getColorString() {
		if (color == Color.BLACK) {
			return "BLACK";
		}

		if (color == Color.BLUE) {
			return "BLUE";
		}

		if (color == Color.RED) {
			return "RED";
		}

		return "WHITE";
	}

	/**
	 * Get the List of neighbours.
	 * 
	 * @return the List of neighbours.
	 */
	public List<Edge> getNeighbours() {
		return neighbours;
	}

	/**
	 * Get a array with the eight neighbour indice
	 * |x-1,y-1|x,y-1|x+1,y-1|
	 * |x-1,y  |x,y  |x+1,y  |
	 * |x-1,y+1|x,y+1|x+1,y+1|
	 * 
	 * @param xLength
	 * 		length of the line
	 * 
	 * @return
	 * 		eight neighbour indice
	 */
	private int[] getIndiceEightNeighbour(int xLength) {
		int[] indices = { ((y - 1) * xLength) + (x - 1),
				((y - 1) * xLength) + x, ((y - 1) * xLength) + (x + 1),

				(y * xLength) + (x - 1), (y * xLength) + (x + 1),

				((y + 1) * xLength) + (x - 1), ((y + 1) * xLength) + x,
				((y + 1) * xLength) + (x + 1), };
		return indices;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	/**
	 * Check if the vertex has a given vertex as a neighbour in the neighbours'
	 * List.
	 * 
	 * @param vertex
	 *            The given Vertex.
	 * @return <code>true</code> if the given vertex is a neighbour of the
	 *         current vertex, <code>false</code> otherwise.
	 */
	/*
	 * public boolean hasNeighbours(Vertex vertex) { if (vertex.getId() < 0) {
	 * throw new IllegalArgumentException(); } return contains(new Edge(this,
	 * vertex)); }
	 */

	/**
	 * Get the degree of a vertex by checking the neighbours' List (Vertex).
	 * 
	 * @return The degree.
	 */
	public int degree() {
		return neighbours.size();
	}

	/**
	 * Check if the edge is contained in the neighbours' List (Vertex).
	 * 
	 * @param edge
	 *            The edge.
	 * @return <code>true</code> if the edge is already in the neighbours' List,
	 *         <code>false</code> otherwise.
	 */
	public boolean contains(Edge edge) {
		return neighbours.contains(edge);
	}

	/**
	 * Add an edge in the neighbours' List (Vertex).
	 * 
	 * @param edge
	 *            The edge.
	 * @return <code>true</code> if the add was a success, <code>false</code>
	 *         otherwise.
	 */
	public boolean addEdge(Edge edge) {
		if (!contains(edge))
			return neighbours.add(edge);
		return false;
	}

	/**
	 * Remove an edge in the neighbours' List (Vertex).
	 * 
	 * @param edge
	 *            The edge.
	 * @return <code>true</code> if the remove was a success, <code>false</code>
	 *         otherwise.
	 */
	public boolean removeEdge(Edge edge) {
		return neighbours.remove(edge);
	}

	/**
	 * Compare two vertex with their distance
	 */
	public int compareTo(Vertex other) {
		return Double.compare(minDistance, other.minDistance);
	}

	/**
	 * Display a vertex into a string
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		sb.append("(" + x + "," + y + ") | ");
		sb.append(getColorString());
		return sb.append("]").toString();
	}

	/**
	 * Get the previous vertex
	 * 
	 * @return
	 * 		previous vertex
	 */
	public Vertex getPrevious() {
		return previous;
	}

	/**
	 * Set the previous vertex
	 * @param previous
	 */
	public void setPrevious(Vertex previous) {
		this.previous = previous;
	}

}
